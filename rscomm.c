#include "rscomm.h"

static device_ctx* create_device_ctx(char* portname){
	device_ctx* ctx=(device_ctx*) malloc(sizeof(device_ctx));
	memset( ctx, 0, sizeof( device_ctx ) );
	// Allocate ctx:
	ctx->dcb=(DCB*) malloc(sizeof(DCB));
	memset(ctx->dcb, 0, sizeof(DCB));
	ctx->xdcb=(dcb_user*) malloc(sizeof(dcb_user));
	memset(ctx->xdcb, 0, sizeof(dcb_user));
	ctx->timeouts=(COMMTIMEOUTS*) malloc(sizeof(COMMTIMEOUTS));
	memset(ctx->timeouts, 0, sizeof(COMMTIMEOUTS));
	
	ctx->in_stream=(iobuffer*) malloc(sizeof(iobuffer));
	ctx->in_stream->data=(char*) malloc(rxbuff_len*sizeof(char));
	memset(ctx->in_stream->data, 0, rxbuff_len*sizeof(char));    
	ctx->in_stream->len=rxbuff_len;

	ctx->out_stream=(iobuffer*) malloc(sizeof(iobuffer));
	ctx->out_stream->data=(char*) malloc(txbuff_len*sizeof(char));
	memset(ctx->out_stream->data, 0, txbuff_len*sizeof(char)); 
	ctx->out_stream->len=txbuff_len;
	
	// assign name
	strcpy(ctx->name, portname);
	return ctx;
}

RSCOMM device_ctx*  rscomm_open( char* portname ){
	device_ctx* ctx = create_device_ctx( portname );
#if defined(RUNTEST)
	printf("Com port to open: [");
	printf(ctx->name);
	printf("]\n");
#endif
	ctx->hdevice = CreateFile(ctx->name, 
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE, // former 0
		0,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, // half-duplex or sync mode
		0);

	if (ctx->hdevice != NULL && ctx->hdevice != INVALID_HANDLE_VALUE){
		// fill dcb struct:
		if(!GetCommState(ctx->hdevice, ctx->dcb)) return NULL;
		ctx->async=FALSE;
		return ctx;
	}
	else
		return NULL;
}
	
RSCOMM device_ctx* rscomm_async_open( char* portname ){
	device_ctx* ctx = create_device_ctx( portname );
	ctx->hdevice = CreateFile(ctx->name, 
		GENERIC_READ | GENERIC_WRITE,
		0,
		0,
		OPEN_EXISTING,
		FILE_FLAG_OVERLAPPED, // full-duplex or async mode
		0);
		
	if (ctx->hdevice != NULL && ctx->hdevice != INVALID_HANDLE_VALUE){
		// fill dcb struct:
		if(!GetCommState(ctx->hdevice, ctx->dcb)) return NULL;
		ctx->poverlapped_in=(LPOVERLAPPED) malloc(sizeof(OVERLAPPED));
		ctx->poverlapped_out=(LPOVERLAPPED) malloc(sizeof(OVERLAPPED));
		ctx->async=TRUE;
		// ... create threads here?
		return ctx;
	}
	else
		return NULL;
}
	
RSCOMM BOOL rscomm_close( device_ctx* ctx ){
	
	if(ctx->h_writer_thread){
		TerminateThread(ctx->h_writer_thread,0);
		CloseHandle(ctx->h_writer_thread);
	}
	if(ctx->poverlapped_out && ctx->poverlapped_out->hEvent) CloseHandle(ctx->poverlapped_out->hEvent);
	free(ctx->poverlapped_out);
	
	if(ctx->h_reader_thread){
		TerminateThread(ctx->h_reader_thread,0);
		CloseHandle(ctx->h_reader_thread);
	}
	if(ctx->poverlapped_in && ctx->poverlapped_in->hEvent) CloseHandle(ctx->poverlapped_in->hEvent);
	free(ctx->poverlapped_in);

	BOOL result=CloseHandle(ctx->hdevice);  //close port
	ctx->hdevice=0;		//�������� ���������� ��� ����������� �����

	free(ctx->dcb);
	free(ctx->xdcb);
	free(ctx->timeouts);
	free(ctx->in_stream->data);
	free(ctx->in_stream);
	free(ctx->out_stream->data);
	free(ctx->out_stream);
	free(ctx);
	return result;
}

RSCOMM BOOL rscomm_read( device_ctx* ctx, iobuffer* pbuffer, DWORD btoread ){
	// to-do: what if btoread=pbuffer->len? continue to read until.. and concat result
	DWORD errors;
	COMSTAT commstat;
	if( btoread > rxbuff_len ) btoread=rxbuff_len;
	memset( pbuffer->data, 0, pbuffer->len );
	pbuffer->len=0;
	//~ DWORD event_mask = EV_RXCHAR;
	//~ WaitCommEvent(hCommDev, &event_mask, NULL);
	ClearCommError(ctx->hdevice, &errors, &commstat);
	if (commstat.cbInQue > 0) {
		btoread = (commstat.cbInQue > rxbuff_len) ? rxbuff_len : commstat.cbInQue;
		return ReadFile(ctx->hdevice, pbuffer->data, btoread, ((PDWORD)&(pbuffer->len)), ctx->poverlapped_in);
	} else
	return TRUE;	
}
	
RSCOMM BOOL rscomm_write(device_ctx* ctx, iobuffer* pbuffer){
	// to-do: what if number of char~s to write > txbuff_len?
	// to-do: what if number of char~s sent < char~s to write? continue until.. and truncate sent buffer
	BOOL result=TRUE;
	DWORD b_written=0;
#if defined(RUNTEST)
	printf("rscomm_write: 1...\n");
#endif	
	DWORD event_mask=ctx->wait_mask;
	char* data=pbuffer->data;
	size_t len=pbuffer->len;
#if defined(RUNTEST)
	printf("rscomm_write: about to call EscapeCommFunction...\n");
#endif	
	// for manual control - set dcb->fRtsControl =false 
	if(0x0==ctx->dcb->fRtsControl && ctx->xdcb->force_rts_sw_control) 
		EscapeCommFunction(ctx->hdevice, SETRTS);
#if defined(RUNTEST)
	printf("rscomm_write: EscapeCommFunction passed...\n");
#endif
	result = WriteFile(ctx->hdevice, data, len, &b_written, ctx->poverlapped_out );
#if defined(RUNTEST)
	printf("rscomm_write: data sent, starting WaitCommEvent...\n");
#endif
	if(result && event_mask) WaitCommEvent(ctx->hdevice, &event_mask, ctx->poverlapped_out );
	// for manual control - set dcb->fRtsControl =false 
	if(0x0==ctx->dcb->fRtsControl && ctx->xdcb->force_rts_sw_control) 
		EscapeCommFunction(ctx->hdevice, CLRRTS);
	return result;	// -1 on error	
}

RSCOMM unsigned char* rscomm_read_str( device_ctx* ctx, unsigned char* data, size_t* btoread ){
	// to-do: what if btoread=pbuffer->len? continue to read until.. and concat result
	DWORD errors;
	COMSTAT commstat;
	size_t expected_len= *btoread;
	if( expected_len > rxbuff_len ) expected_len=rxbuff_len;
	memset( data, 0, expected_len );
	*btoread=0;
	//~ DWORD event_mask = EV_RXCHAR;
	//~ WaitCommEvent(hCommDev, &event_mask, NULL);
	ClearCommError(ctx->hdevice, &errors, &commstat);
	if (commstat.cbInQue > 0) {
		expected_len = (commstat.cbInQue > rxbuff_len) ? rxbuff_len : commstat.cbInQue;
		if(!ReadFile(ctx->hdevice, data, expected_len, btoread, ctx->poverlapped_in)) {
			*btoread=0;
			return NULL;
		}
	}
	return data;	
}
	
RSCOMM signed int rscomm_write_str(device_ctx* ctx, unsigned char* data, size_t btowrite){
	// to-do: what if number of char~s to write > txbuff_len?
	// to-do: what if number of char~s sent < char~s to write? continue until.. and truncate sent buffer
	size_t b_written=-1;
	BOOL explicit_rts=(!(ctx->dcb->fRtsControl) && ctx->xdcb->force_rts_sw_control);
	DWORD event_mask=ctx->wait_mask;
	if( btowrite>txbuff_len ) btowrite=txbuff_len;
#if defined(RUNTEST)
	printf("rscomm_write: about to call EscapeCommFunction...\n");
#endif	
	// for manual control - set dcb->fRtsControl =false 
	if(explicit_rts) EscapeCommFunction(ctx->hdevice, SETRTS);
#if defined(RUNTEST)
	printf("rscomm_write: EscapeCommFunction passed...\n");
#endif
	if(WriteFile(ctx->hdevice, data, btowrite, &b_written, ctx->poverlapped_out ) && event_mask) WaitCommEvent(ctx->hdevice, &event_mask, ctx->poverlapped_out );
#if defined(RUNTEST)
	printf("rscomm_write: data sent, starting WaitCommEvent...\n");
#endif
	// for manual control - set dcb->fRtsControl =false 
	if(explicit_rts)	EscapeCommFunction(ctx->hdevice, CLRRTS);
	return (b_written-btowrite);	// <0 on error, 0 on success	
}

RSCOMM BOOL rscomm_flush(device_ctx* ctx){
	return FlushFileBuffers(ctx->hdevice);
}

RSCOMM DCB* rscomm_start_dcb_settings( device_ctx* ctx ){
	return ctx->dcb;
}
	
RSCOMM BOOL rscomm_apply_dcb_settings( device_ctx* ctx ){
	return SetCommState( ctx->hdevice, (LPDCB)(ctx->dcb) );
}

RSCOMM void rscomm_set_baudrate( device_ctx* ctx, DWORD baudrate ){
	ctx->dcb->BaudRate = baudrate;
}// DCB members

RSCOMM void rscomm_set_binary( device_ctx* ctx, DWORD binary ){
	ctx->dcb->fBinary = binary;
}// DCB members

RSCOMM void rscomm_set_fparity( device_ctx* ctx, DWORD parity ){
	ctx->dcb->fParity = parity;
}// DCB members

RSCOMM void rscomm_set_outx_cts_flow( device_ctx* ctx, DWORD outx_cts_flow ){
	ctx->dcb->fOutxCtsFlow = outx_cts_flow;
}// DCB members

RSCOMM void rscomm_set_outx_dsr_flow( device_ctx* ctx, DWORD outx_dsr_flow ){
	ctx->dcb->fOutxDsrFlow = outx_dsr_flow;
}// DCB members

RSCOMM void rscomm_set_dtr_control( device_ctx* ctx, DWORD dtr_control ){
	ctx->dcb->fDtrControl = dtr_control;
}// DCB members

RSCOMM void rscomm_set_dsr_sensitivity( device_ctx* ctx, DWORD dsr_sensitivity ){
	ctx->dcb->fDsrSensitivity = dsr_sensitivity;
}// DCB members

RSCOMM void rscomm_set_tx_continue_on_xoff( device_ctx* ctx, DWORD tx_continue_on_xoff ){
	ctx->dcb->fTXContinueOnXoff = tx_continue_on_xoff;
}// DCB members

RSCOMM void rscomm_set_out_x( device_ctx* ctx, DWORD out_x ){
	ctx->dcb->fOutX = out_x;
}// DCB members

RSCOMM void rscomm_set_in_x( device_ctx* ctx, DWORD in_x ){
	ctx->dcb->fInX = in_x;
}// DCB members

RSCOMM void rscomm_set_flag_error_char( device_ctx* ctx, DWORD flag_error_char ){
	ctx->dcb->fErrorChar = flag_error_char;
}// DCB members

RSCOMM void rscomm_set_flag_null( device_ctx* ctx, DWORD flag_null ){
	ctx->dcb->fNull = flag_null;
}// DCB members

RSCOMM void rscomm_set_rts_control( device_ctx* ctx, DWORD rts_control ){
	ctx->dcb->fRtsControl = rts_control;
}// DCB members

RSCOMM void rscomm_set_abort_on_error( device_ctx* ctx, DWORD abort_on_error ){
	ctx->dcb->fAbortOnError = abort_on_error;
}// DCB members


RSCOMM void rscomm_set_xon_lim( device_ctx* ctx, WORD xon_lim ){
	ctx->dcb->XonLim = xon_lim;
}// DCB members

RSCOMM void rscomm_set_xoff_lim( device_ctx* ctx, WORD xoff_lim ){
	ctx->dcb->XoffLim = xoff_lim;
}// DCB members

RSCOMM void rscomm_set_bytesize( device_ctx* ctx, BYTE bsize ){
	ctx->dcb->ByteSize = bsize;
}// DCB members

RSCOMM void rscomm_set_parity( device_ctx* ctx, BYTE parity ){
	ctx->dcb->Parity = parity;
}// DCB members

RSCOMM void rscomm_set_stop_bits( device_ctx* ctx, BYTE stop_bits ){
	ctx->dcb->StopBits = stop_bits;
}// DCB members

RSCOMM void rscomm_set_xon_char( device_ctx* ctx, char xon_char ){
	ctx->dcb->XonChar = xon_char;
}// DCB members

RSCOMM void rscomm_set_xoff_char( device_ctx* ctx, char xoff_char ){
	ctx->dcb->XoffChar = xoff_char;
}// DCB members

RSCOMM void rscomm_set_error_char( device_ctx* ctx, char error_char ){
	ctx->dcb->ErrorChar = error_char;
}// DCB members

RSCOMM void rscomm_set_eof_char( device_ctx* ctx, char eof_char ){
	ctx->dcb->EofChar = eof_char;
}// DCB members

RSCOMM void rscomm_set_evt_char( device_ctx* ctx, char evt_char ){
	ctx->dcb->EvtChar = evt_char;
}// DCB members

/* 2 */
	
RSCOMM BOOL rscomm_set_rx_timeouts( device_ctx* ctx, signed long interval, signed long multiplier, signed long constant){
	COMMTIMEOUTS timeouts;
	int result=GetCommTimeouts( ctx->hdevice, &timeouts);
	if( result ){
		if(interval>-1) timeouts.ReadIntervalTimeout=interval; // (between bytes)
		if(multiplier>-1) timeouts.ReadTotalTimeoutMultiplier=multiplier; // (per 1 char)
		if(constant>-1) timeouts.ReadTotalTimeoutConstant=constant; // (additional time)
		result=SetCommTimeouts( ctx->hdevice,  &timeouts);
	// Copy timeouts to ctx if settings applied
	if (result) (*ctx->timeouts)=timeouts;
	}
	return result;
} 

RSCOMM BOOL rscomm_set_tx_timeouts( device_ctx* ctx, signed long multiplier, signed long constant){
	COMMTIMEOUTS timeouts;
	int result=GetCommTimeouts( ctx->hdevice, &timeouts);
	if( result ){
		if(multiplier>-1) timeouts.WriteTotalTimeoutMultiplier=multiplier; // (per 1 char)
		if(constant>-1) timeouts.WriteTotalTimeoutConstant=constant; // (additional time)
		result=SetCommTimeouts( ctx->hdevice,  &timeouts);
	// Copy timeouts to ctx if settings applied
	if (result) (*ctx->timeouts)=timeouts;
	}
	return result;
}

RSCOMM BOOL rscomm_set_wait_mask( device_ctx* ctx,  DWORD wait_mask){
	if( SetCommMask( ctx->hdevice, wait_mask ) ){
		ctx->wait_mask=wait_mask;
		return TRUE;
	}
	else
		return FALSE;
}
/*
EV_BREAK (0x0040) A break was detected on input; 
EV_CTS (0x0008) The CTS (clear-to-send) signal changed state; 
EV_DSR (0x0010) The DSR (data-set-ready) signal changed state; 
EV_ERR (0x0080) A line-status error occurred. Line-status errors are CE_FRAME, CE_OVERRUN, and CE_RXPARITY; 
EV_RING (0x0100) A ring indicator was detected; 
EV_RLSD (0x0020) The RLSD (receive-line-signal-detect) signal changed state;
EV_RXCHAR (0x0001) A character was received and placed in the input buffer;
EV_RXFLAG (0x0002) The event character was received and placed in the input buffer. The event character is specified in the device's DCB structure, which is applied to a serial port by using the SetCommState function; 
EV_TXEMPTY (0x0004) The last character in the output buffer was sent. 
*/

RSCOMM BOOL rscomm_set_queue_len( device_ctx* ctx, DWORD qrx, DWORD qtx ){
	if( SetupComm( ctx->hdevice, qrx, qtx) ){
		ctx->rx_queue_len=qrx;
		ctx->tx_queue_len=qtx;
		//~ if(ctx->in_stream->data) free(ctx->in_stream->data);
		//~ ctx->in_stream->data=(char*) malloc(rxbuff_len*sizeof(char));
		
		return TRUE;
	}
	else
		return FALSE;
}


RSCOMM BOOL rscomm_purge( device_ctx* ctx,  DWORD flags){
	return PurgeComm( ctx->hdevice, flags );
}

	
	
	
// UTILS:
	
#if defined(RS_BUILD_STATIC)
	#include <stdio.h>
	
	int showError( char* message ){
			char lastError[1024];
			if(message) { printf(message); printf("\n"); }
			
			FormatMessage(
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				lastError,
				1024,
				NULL);
			printf(lastError);
			return (BOOL)0;
		}

	void dump_dcb(DCB* dcb){
		//~ printf(" 0x%x",  dcb->DCBlength);  
		printf("DCB dump:-----------------------------\n");
		printf("BaudRate 0x%x\n",  dcb->BaudRate);  
		printf("fBinary 0x%x\n",  dcb->fBinary);  
		printf("fParity 0x%x\n",  dcb->fParity);  
		printf("fOutxCtsFlow 0x%x\n",  dcb->fOutxCtsFlow);  
		printf("fOutxDsrFlow 0x%x\n",  dcb->fOutxDsrFlow);  
		printf("fDtrControl 0x%x\n",  dcb->fDtrControl);  
		printf("fDsrSensitivity 0x%x\n",  dcb->fDsrSensitivity);  
		printf("fTXContinueOnXoff 0x%x\n",  dcb->fTXContinueOnXoff);  
		printf("fOutX 0x%x\n",  dcb->fOutX);  
		printf("fInX 0x%x\n",  dcb->fInX);  
		printf("fErrorChar 0x%x\n",  dcb->fErrorChar);  
		printf("fNull 0x%x\n",  dcb->fNull);  
		printf("fRtsControl 0x%x\n",  dcb->fRtsControl);  
		printf("fAbortOnError 0x%x\n",  dcb->fAbortOnError);  
		printf("fDummy2 0x%x\n",  dcb->fDummy2);  
		printf("wReserved 0x%x\n",  dcb->wReserved);  
		printf("XonLim 0x%x\n",  dcb->XonLim);  
		printf("XoffLim 0x%x\n",  dcb->XoffLim);  
		printf("ByteSize 0x%x\n",  dcb->ByteSize);  
		printf("Parity 0x%x\n",  dcb->Parity);  
		printf("StopBits 0x%x\n",  dcb->StopBits);  
		printf("XonChar 0x%x\n",  dcb->XonChar);  
		printf("XoffChar 0x%x\n",  dcb->XoffChar);  
		printf("ErrorChar 0x%x\n",  dcb->ErrorChar);  
		printf("EofChar 0x%x\n",  dcb->EofChar);  
		printf("EvtChar 0x%x\n",  dcb->EvtChar);  
		printf("wReserved1 0x%x\n",  dcb->wReserved1);
		printf("end of DCB dump -----------------------\n");
	}
#endif	
	
#if defined(RUNTEST)

int main(int argc, char** argv){
	// open port

	device_ctx *ctx=rscomm_open("COM1");
	if(!ctx){
		showError("Error opening port\n");
		goto terminate;
	}

	printf("Port opened(!)\n");
	
	rscomm_flush(ctx);
	
	printf("FlushFileBuffers done\n");

	// set up
	DCB* dcb=rscomm_start_dcb_settings(ctx);

	printf("old DCB:\n");
	dump_dcb(ctx->dcb);
	
	printf("rscomm_start_dcb_settings\n");
	rscomm_set_baudrate(ctx, 28800);
	printf("rscomm_set_baudrate\n");
	
	dcb->Parity=NOPARITY;
	printf("parity assign~ed\n");

	if(!rscomm_apply_dcb_settings(ctx)) {
		showError("Error configuring port\n");
		goto terminate;
	}
	printf("Settings - done\n");
	
	printf("New DCB:\n");
	dump_dcb(ctx->dcb);
	
	printf("Prepare data to send...\n");
	
	char command[10]="\x30\x31\x32\x30";
	iobuffer buff;
	buff.data=command;
	buff.len=4;
	
	printf("Prepare to write...\n");
	
	if(!rscomm_write(ctx, &buff)){
		showError("Error writing to port\n");
		goto terminate;
	};
	
	signed int b_written=rscomm_write_str(ctx, buff.data, buff.len);
	printf("rscomm_write_str: %d bytes written\n---<", b_written);
	printf(buff.data);
	printf(">---\n");
	
	char inbuff[100]={0};
	rscomm_read_str(ctx, inbuff, &buff.len);
	printf("rscomm_read_str: %d bytes read\n---<", buff.len);
	printf(inbuff);
	printf(">---\n");

printf("Write - done\n");
		
	// write
terminate:
	// close
	if(!rscomm_close(ctx)) showError("Error closing port\n"); else printf("Closing - done\n");
}
#else if defined(WIN32) && defined(L_XTENSION) && !defined(RS_BUILD_STATIC) 
__declspec(dllexport) BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) 
{
	return TRUE;
}
#endif
#if defined(RUN_PROTO) && defined(RS_BUILD_STATIC)

#define CMD(ctx, data) \
	{\
		char buff[]=data;\
		size_t len=sizeof data;\
		size_t cmd_len=len;\
		if(!rscomm_write(ctx, buff, &len)) printf("Cannot send command\n");\
		if(len!=cmd_len) printf("Command broken: %d bytes of %d was sent\n", len, cmd_len);\
	}


int main(int argc, char** argv){
	device_ctx *ctx=rscomm_open("COM1");
	if(!ctx){
		showError("Error opening port\n");
		goto terminate;
	}

	printf("Port opened(!)\n");
	
	rscomm_flush(ctx);
	
	printf("FlushFileBuffers done\n");

	// set up
	DCB* dcb=rscomm_start_dcb_settings(ctx);

	printf("old DCB:\n");
	dump_dcb(ctx->dcb);
	
	printf("rscomm_start_dcb_settings\n");
	rscomm_set_baudrate(ctx, 28800);
	printf("rscomm_set_baudrate\n");
	
	dcb->Parity=NOPARITY;
	printf("parity assign~ed\n");

	if(!rscomm_apply_dcb_settings(ctx)) {
		showError("Error configuring port\n");
		goto terminate;
	}
	printf("Settings - done\n");
	
	printf("New DCB:\n");
	dump_dcb(ctx->dcb);
	
	printf("Prepare data to send...\n");


	CMD(ctx, {0xFF})
	CMD(ctx, {0x31,0x05})

terminate:
	// close
	if(!rscomm_close(ctx)) showError("Error closing port\n"); else printf("Closing - done\n");
	return 0;
}
#endif

#define WIN32

#if defined(WIN32)
	#include <windows.h>
#endif

#include <string.h>

#if defined(WIN32) && !defined(RS_BUILD_STATIC)
	#ifdef RS_BUILD_EXPORT
		#define RSCOMM __declspec(dllexport)
	#else
		#define RSCOMM __declspec(dllimport)
	#endif
#else
	#define RSCOMM
#endif

/*

to-do:
use simple standard types vs BOOL, DWORD, WINAPI, ...

*/

size_t txbuff_len=5000;
size_t rxbuff_len=5000;

typedef struct
{
	char *data;
	size_t len;
} iobuffer;

typedef struct {
	BOOL force_rts_sw_control; // send RTS control signal from io routines using EscapeCommFunction; note that fRtsControl of dcb must be 0 in order to take effect
} dcb_user;
	
typedef struct tag_device_ctx {
	char 			name[7];
	HANDLE 		hdevice; // port handle
	BOOL 			async;
	DCB 			*dcb; 
	dcb_user 		*xdcb;
	COMMTIMEOUTS 		*timeouts;
	DWORD 		wait_mask;
	DWORD 		rx_queue_len;
	DWORD 		tx_queue_len;
	HANDLE 		h_reader_thread; // for async mode=  thread descriptor for read, if async mode select~ed, not implemented
	HANDLE 		h_writer_thread;  // for async mode= thread descriptor for write, if async mode select~ed, not implemented
	iobuffer 			*in_stream;
	iobuffer 			*out_stream;
	LPOVERLAPPED poverlapped_in;		//for async mode
	LPOVERLAPPED poverlapped_out;       	//for async mode
} device_ctx;

RSCOMM device_ctx*  rscomm_open(char* portname);
RSCOMM device_ctx*  rscomm_async_open(char* portname); // not implemented
RSCOMM BOOL rscomm_close( device_ctx* ctx );

RSCOMM BOOL rscomm_read( device_ctx* ctx, iobuffer* pbuffer, DWORD btoread );
RSCOMM BOOL rscomm_write(device_ctx* ctx, iobuffer *pbuffer);
//---
RSCOMM unsigned char* rscomm_read_str( device_ctx* ctx, unsigned char* data, size_t* btoread );
RSCOMM signed int rscomm_write_str(device_ctx* ctx, unsigned char* data, size_t btowrite);
//---
RSCOMM BOOL rscomm_flush(device_ctx* ctx);

RSCOMM DCB* rscomm_start_dcb_settings( device_ctx* ctx );
/*
//use cases:
	DCB dcb=rscomm_start_dcb_settings(o);
	dcb.BaudRate=BAUDRATE;
	dcb.ByteSize=WORD_LENGTH;
	dcb.StopBits=STOP_BITS;
	dcb.Parity=PARITY;
	rscomm_apply_dcb_settings(o);
//OR:
	rscomm_set_baudrate(o, BAUDRATE);
	rscomm_set_bytesize(o, WORD_LENGTH);
	rscomm_apply_dcb_settings(o);
// Same paradigm with COMMTIMEOUTS...
*/
RSCOMM BOOL rscomm_apply_dcb_settings( device_ctx* ctx ); // always call after dcb settings

RSCOMM void rscomm_set_baudrate( device_ctx* ctx, DWORD baudrate );
RSCOMM void rscomm_set_binary( device_ctx* ctx, DWORD binary ); // If is TRUE, binary mode is enabled. Windows does not support nonbinary mode transfers, so must be TRUE. 
RSCOMM void rscomm_set_fparity( device_ctx* ctx, DWORD parity ); // TRUE/FALSE
RSCOMM void rscomm_set_outx_cts_flow( device_ctx* ctx, DWORD outx_cts_flow ); // If is TRUE, the CTS (clear-to-send) signal is monitored for output flow control. If is TRUE and CTS is turned off, output is suspended until CTS is sent again.
RSCOMM void rscomm_set_outx_dsr_flow( device_ctx* ctx, DWORD outx_dsr_flow ); // If is TRUE, the DSR (data-set-ready) signal is monitored for output flow control. If is TRUE and DSR is turned off, output is suspended until DSR is sent again. 
RSCOMM void rscomm_set_dtr_control( device_ctx* ctx, DWORD dtr_control ); // DTR (data-terminal-ready) flow control. Can be one of the following values : DTR_CONTROL_DISABLE (0x00 Disables the DTR line when the device is opened and leaves it disabled; DTR_CONTROL_ENABLE (0x01) Enables the DTR line when the device is opened and leaves it on. DTR_CONTROL_HANDSHAKE (0x02) Enables DTR handshaking. If handshaking is enabled, it is an error for the application to adjust the line by using the EscapeCommFunction function. 
RSCOMM void rscomm_set_dsr_sensitivity( device_ctx* ctx, DWORD dsr_sensitivity ); // If is TRUE, the communications driver is sensitive to the state of the DSR signal. The driver ignores any bytes received, unless the DSR modem input line is high. 
RSCOMM void rscomm_set_tx_continue_on_xoff( device_ctx* ctx, DWORD tx_continue_on_xoff ); // If is TRUE, transmission continues after the input buffer has come within XoffLim bytes of being full and the driver has transmitted the XoffChar character to stop receiving bytes. If is FALSE, transmission does not continue until the input buffer is within XonLim bytes of being empty and the driver has transmitted the XonChar character to resume reception. 
RSCOMM void rscomm_set_out_x( device_ctx* ctx, DWORD out_x ); // Indicates whether XON/XOFF flow control is used during transmission. If is TRUE, transmission stops when the XoffChar character is received and starts again when the XonChar character is received.
RSCOMM void rscomm_set_in_x( device_ctx* ctx, DWORD in_x ); // Indicates whether XON/XOFF flow control is used during reception. If is TRUE, the XoffChar character is sent when the input buffer comes within XoffLim bytes of being full, and the XonChar character is sent when the input buffer comes within XonLim bytes of being empty.
RSCOMM void rscomm_set_flag_error_char( device_ctx* ctx, DWORD flag_error_char ); // Indicates whether bytes received with parity errors are replaced with the character specified by the ErrorChar member. If is TRUE and the fParity member is TRUE, replacement occurs. 
RSCOMM void rscomm_set_flag_null( device_ctx* ctx, DWORD flag_null ); // If is TRUE, null bytes are discarded when received.
RSCOMM void rscomm_set_rts_control( device_ctx* ctx, DWORD rts_control ); // RTS (request-to-send) flow control. can be one of the following values. RTS_CONTROL_DISABLE (0x00) Disables the RTS line when the device is opened and leaves it disabled; RTS_CONTROL_ENABLE (0x01) Enables the RTS line when the device is opened and leaves it on; RTS_CONTROL_HANDSHAKE (0x02) Enables RTS handshaking. The driver raises the RTS line when the "type-ahead" (input) buffer is less than one-half full and lowers the RTS line when the buffer is more than three-quarters full. If handshaking is enabled, it is an error for the application to adjust the line by using the EscapeCommFunction function; RTS_CONTROL_TOGGLE (0x03) Specifies that the RTS line will be high if bytes are available for transmission. After all buffered bytes have been sent, the RTS line will be low. 
RSCOMM void rscomm_set_abort_on_error( device_ctx* ctx, DWORD abort_on_error ); // If is TRUE, the driver terminates all read and write operations with an error status if an error occurs. The driver will not accept any further communications operations until the application has acknowledged the error by calling the ClearCommError function.
RSCOMM void rscomm_set_xon_lim( device_ctx* ctx, WORD xon_lim ); // Minimum number of bytes allowed in the input buffer before flow control is activated to inhibit the sender. Note that the sender may transmit characters after the flow control signal has been activated, so this value should never be zero. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in fInX, fRtsControl, or fDtrControl.
RSCOMM void rscomm_set_xoff_lim( device_ctx* ctx, WORD xoff_lim ); // Maximum number of bytes allowed in the input buffer before flow control is activated to allow transmission by the sender. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in fInX, fRtsControl, or fDtrControl. The maximum number of bytes allowed is calculated by subtracting this value from the size, in bytes, of the input buffer.
RSCOMM void rscomm_set_bytesize( device_ctx* ctx, BYTE bsize ); // Number of bits in the bytes transmitted and received. 
RSCOMM void rscomm_set_parity( device_ctx* ctx, BYTE parity ); // EVENPARITY (2) Even parity; MARKPARITY (3) Mark parity; NOPARITY (0) No parity; ODDPARITY (1) Odd parity; SPACEPARITY (4) Space parity. 
RSCOMM void rscomm_set_stop_bits( device_ctx* ctx, BYTE stop_bits ); // ONESTOPBIT (0) 1 stop bit; ONE5STOPBITS (1) 1.5 stop bits; TWOSTOPBITS (2) 2 stop bits.

RSCOMM void rscomm_set_xon_char( device_ctx* ctx, char xon_char ); // Value of the XON character for both transmission and reception. 
RSCOMM void rscomm_set_xoff_char( device_ctx* ctx, char xoff_char ); // Value of the XOFF character for both transmission and reception.
RSCOMM void rscomm_set_error_char( device_ctx* ctx, char error_char ); // Value of the character used to replace bytes received with a parity error.
RSCOMM void rscomm_set_eof_char( device_ctx* ctx, char eof_char ); // Value of the character used to signal the end of data.
RSCOMM void rscomm_set_evt_char( device_ctx* ctx, char evt_char ); // Value of the character used to signal an event.

RSCOMM BOOL rscomm_set_rx_timeouts( device_ctx* ctx, signed long interval, signed long multiplier, signed long constant); 
/*
ReadIntervalTimeout (between bytes)
ReadTotalTimeoutMultiplier (per 1 byte)
ReadTotalTimeoutConstant (additional time)
*/

RSCOMM BOOL rscomm_set_tx_timeouts( device_ctx* ctx, signed long multiplier, signed long constant); 
/*
WriteTotalTimeoutMultiplier (per 1 byte)
WriteTotalTimeoutConstant (additional time)
*/

RSCOMM BOOL rscomm_set_wait_mask( device_ctx* ctx,  DWORD wait_mask);
/*
EV_BREAK (0x0040) A break was detected on input; 
EV_CTS (0x0008) The CTS (clear-to-send) signal changed state; 
EV_DSR (0x0010) The DSR (data-set-ready) signal changed state; 
EV_ERR (0x0080) A line-status error occurred. Line-status errors are CE_FRAME, CE_OVERRUN, and CE_RXPARITY; 
EV_RING (0x0100) A ring indicator was detected; 
EV_RLSD (0x0020) The RLSD (receive-line-signal-detect) signal changed state;
EV_RXCHAR (0x0001) A character was received and placed in the input buffer;
EV_RXFLAG (0x0002) The event character was received and placed in the input buffer. The event character is specified in the device's DCB structure, which is applied to a serial port by using the SetCommState function; 
EV_TXEMPTY (0x0004) The last character in the output buffer was sent. 
*/


RSCOMM BOOL rscomm_set_queue_len( device_ctx* ctx, DWORD qrx, DWORD qtx );


RSCOMM BOOL rscomm_purge( device_ctx* ctx,  DWORD flags);
/*
PURGE_RXABORT (0x0002) Terminates all outstanding overlapped read operations and returns immediately, even if the read operations have not been completed. 
PURGE_RXCLEAR (0x0008) Clears the input buffer (if the device driver has one). 
PURGE_TXABORT (0x0001) Terminates all outstanding overlapped write operations and returns immediately, even if the write operations have not been completed. 
PURGE_TXCLEAR (0x0004) Clears the output buffer (if the device driver has one). 
*/

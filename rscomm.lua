--rscomm import

local ffi=assert(require"ffi", "LuaJIT FFI necessary for this code.");

ffi.cdef[[

void Sleep(int ms);

void *malloc(size_t size);
void free(void *ptr);

void*  rscomm_open(uint8_t* portname);
void*  rscomm_async_open(uint8_t* portname);
int rscomm_close( void* ctx );

//---
uint8_t* rscomm_read_str( void* ctx, uint8_t* data, size_t* btoread );
signed int rscomm_write_str(void* ctx, uint8_t* data, size_t btowrite);
//---

int rscomm_flush(void* ctx);
int rscomm_purge( void* ctx,  unsigned long flags); // see [1]

int rscomm_apply_dcb_settings( void* ctx ); 
void rscomm_set_baudrate( void* ctx, unsigned long baudrate );
void rscomm_set_binary( void* ctx, unsigned long binary );
void rscomm_set_fparity( void* ctx, unsigned long parity ); 
void rscomm_set_outx_cts_flow( void* ctx, unsigned long outx_cts_flow ); 
void rscomm_set_outx_dsr_flow( void* ctx, unsigned long outx_dsr_flow ); 
void rscomm_set_dtr_control( void* ctx, unsigned long dtr_control ); 
void rscomm_set_dsr_sensitivity( void* ctx, unsigned long dsr_sensitivity );  
void rscomm_set_tx_continue_on_xoff( void* ctx, unsigned long tx_continue_on_xoff ); 
void rscomm_set_out_x( void* ctx, unsigned long out_x ); 
void rscomm_set_in_x( void* ctx, unsigned long in_x ); 
void rscomm_set_flag_error_char( void* ctx, unsigned long flag_error_char ); 
void rscomm_set_flag_null( void* ctx, unsigned long flag_null ); 
void rscomm_set_rts_control( void* ctx, unsigned long rts_control ); 
void rscomm_set_abort_on_error( void* ctx, unsigned long abort_on_error ); 
void rscomm_set_xon_lim( void* ctx, unsigned short xon_lim ); 
void rscomm_set_xoff_lim( void* ctx, unsigned short xoff_lim ); 
void rscomm_set_bytesize( void* ctx, unsigned char bsize ); 
void rscomm_set_parity( void* ctx, unsigned char parity );
void rscomm_set_stop_bits( void* ctx, unsigned char stop_bits ); 

void rscomm_set_xon_char( void* ctx, char xon_char );
void rscomm_set_xoff_char( void* ctx, char xoff_char );
void rscomm_set_error_char( void* ctx, char error_char );
void rscomm_set_eof_char( void* ctx, char eof_char );
void rscomm_set_evt_char( void* ctx, char evt_char ); 
int rscomm_set_rx_timeouts( void* ctx, signed long interval, signed long multiplier, signed long constant); 
int rscomm_set_tx_timeouts( void* ctx, signed long multiplier, signed long constant); 
int rscomm_set_wait_mask( void* ctx,  unsigned long wait_mask); 
int rscomm_set_queue_len( void* ctx, unsigned long qrx, unsigned long qtx );

]]

-- constants

--[1]
PURGE_RXABORT =0x0002 
--Terminates all outstanding overlapped read operations and returns immediately, even if the read operations have not been completed. 
PURGE_RXCLEAR=0x0008 
--Clears the input buffer (if the device driver has one). 
PURGE_TXABORT=0x0001 
--Terminates all outstanding overlapped write operations and returns immediately, even if the write operations have not been completed. 
PURGE_TXCLEAR=0x0004 
--Clears the output buffer (if the device driver has one). 

--[2]
DTR_CONTROL_DISABLE=0x00 
--Disables the DTR line when the device is opened and leaves it disabled; 
DTR_CONTROL_ENABLE=0x01 
--Enables the DTR line when the device is opened and leaves it on. 
DTR_CONTROL_HANDSHAKE=0x02 
--Enables DTR handshaking. If handshaking is enabled, it is an error for the application to adjust the line by using the EscapeCommFunction function. 

--[3]
RTS_CONTROL_DISABLE=0x00
--Disables the RTS line when the device is opened and leaves it disabled; 
RTS_CONTROL_ENABLE=0x01
-- Enables the RTS line when the device is opened and leaves it on; 
RTS_CONTROL_HANDSHAKE=0x02
-- Enables RTS handshaking. The driver raises the RTS line when the "type-ahead" (input) buffer is less than one-half full and lowers the RTS line when the buffer is more than three-quarters full. If handshaking is enabled, it is an error for the application to adjust the line by using the EscapeCommFunction function; 
RTS_CONTROL_TOGGLE=0x03 
-- Specifies that the RTS line will be high if unsigned chars are available for transmission. After all buffered unsigned chars have been sent, the RTS line will be low. 

--[4]
EVENPARITY=2-- Even parity; 
MARKPARITY=3 --Mark parity; 
NOPARITY=0 --No parity; 
ODDPARITY=1 --Odd parity; 
SPACEPARITY=4 --Space parity. 
--[5]
ONESTOPBIT=0 --1 stop bit; 
ONE5STOPBITS=1 --1.5 stop bits; 
TWOSTOPBITS=2 --2 stop bits.
--[6]
EV_BREAK=0x0040 -- A break was detected on input; 
EV_CTS=0x0008 -- The CTS=clear-to-send -- signal changed state; 
EV_DSR=0x0010 -- The DSR=data-set-ready -- signal changed state; 
EV_ERR=0x0080 -- A line-status error occurred. Line-status errors are CE_FRAME, CE_OVERRUN, and CE_RXPARITY; 
EV_RING=0x0100 -- A ring indicator was detected; 
EV_RLSD=0x0020 -- The RLSD=receive-line-signal-detect -- signal changed state;
EV_RXCHAR=0x0001 -- A character was received and placed in the input buffer;
EV_RXFLAG=0x0002 -- The event character was received and placed in the input buffer. The event character is specified in the device's DCB structure, which is applied to a serial port by using the SetCommState function; 
EV_TXEMPTY=0x0004 -- The last character in the output buffer was sent. 

TXQLEN=5000-1;
RXQLEN=5000-1;

local rslib=assert( ffi.load("rscomm"), "Cannot load dll")

function sleep(ms)
    ffi.C.Sleep(ms)
end

hport=nil --global

function rs_open(portname)
    local cportname=ffi.new("uint8_t[?]",#portname+1, portname)
    hport=assert(rslib.rscomm_open( cportname) , "Cannot open port")
end

--
local readbuffer=ffi.new("uint8_t[?]",RXQLEN, {0}) --only for sync operations, non-overlapped
--
function rs_read(numbytes)
    local readcount=ffi.new("size_t[1]", numbytes) -- [] one element array to make pointer
    ffi.fill( readbuffer, RXQLEN ) --0
    local status = rslib.rscomm_read_str( hport, readbuffer, readcount )
    if status then
--~ 	    print("Read #"..(readcount[0] or "None"))
	    --[[0]]
--~ 	    return ""
	    return readcount[0]>0 and ffi.string(readbuffer, readcount[0]) or ""	
    else
        print"Read error"
	return ""
    end
end


--
local writebuffer=ffi.new("uint8_t[?]", TXQLEN, {0}) --only for sync operations, non-overlapped
--
function rs_write(bytearray)
    --[[return~s difference of bytes to write and bytes written]]
    local sb=string.char( unpack(bytearray) )
    ffi.fill( writebuffer, TXQLEN ) --0
    ffi.copy( writebuffer, sb, #sb )
    return rslib.rscomm_write_str(hport, writebuffer, #bytearray)
end

function rs_close()
    assert(rslib.rscomm_close( hport ), "Cannot close port")
end


---------------------------------------------
function rs_flush()  assert(rslib.rscomm_flush(hport), "Flush error") end
function rs_purge(flags) assert(rslib.rscomm_purge( hport,  flags), "Purge error") end -- see [1]
function rs_apply_dcb() assert(rslib.rscomm_apply_dcb_settings( hport ), "Error on dcb settings") end --always call after dcb settings
function rs_set_baudrate(baudrate) rslib.rscomm_set_baudrate( hport, baudrate ) end
function rs_set_binary(binary) rslib.rscomm_set_binary( hport, binary ) end -- Windows - always TRUE. 
function rs_set_fparity(parity) rslib.rscomm_set_fparity( hport, parity ) end -- TRUE/FALSE
function rs_set_outx_cts_flow(outx_cts_flow) rslib.rscomm_set_outx_cts_flow( hport, outx_cts_flow ) end -- If is TRUE, the CTS (clear-to-send) signal is monitored for output flow control. If is TRUE and CTS is turned off, output is suspended until CTS is sent again.
function rs_set_outx_dsr_flow(outx_dsr_flow) rslib.rscomm_set_outx_dsr_flow( hport, outx_dsr_flow ) end -- If is TRUE, the DSR (data-set-ready) signal is monitored for output flow control. If is TRUE and DSR is turned off, output is suspended until DSR is sent again. 
function rs_set_dtr_control(dtr_control) rslib.rscomm_set_dtr_control( hport, dtr_control ) end -- see [2] DTR (data-terminal-ready) flow control. 
function rs_set_dsr_sensitivity(dsr_sensitivity) rslib.rscomm_set_dsr_sensitivity( hport, dsr_sensitivity ) end -- If is TRUE, the communications driver is sensitive to the state of the DSR signal. The driver ignores any unsigned chars received, unless the DSR modem input line is high. 
function rs_set_tx_continue_on_xoff(tx_continue_on_xoff) rslib.rscomm_set_tx_continue_on_xoff( hport, tx_continue_on_xoff ) end -- If is TRUE, transmission continues after the input buffer has come within XoffLim unsigned chars of being full and the driver has transmitted the XoffChar character to stop receiving unsigned chars. If is FALSE, transmission does not continue until the input buffer is within XonLim unsigned chars of being empty and the driver has transmitted the XonChar character to resume reception. 
function rs_set_out_x(out_x) rslib.rscomm_set_out_x( hport, out_x ) end -- Indicates whether XON/XOFF flow control is used during transmission. If is TRUE, transmission stops when the XoffChar character is received and starts again when the XonChar character is received.
function rs_set_in_x(in_x) rslib.rscomm_set_in_x( hport, in_x ) end -- Indicates whether XON/XOFF flow control is used during reception. If is TRUE, the XoffChar character is sent when the input buffer comes within XoffLim unsigned chars of being full, and the XonChar character is sent when the input buffer comes within XonLim unsigned chars of being empty.
function rs_set_flag_error_char(flag_error_char) rslib.rscomm_set_flag_error_char( hport, flag_error_char ) end -- Indicates whether unsigned chars received with parity errors are replaced with the character specified by the ErrorChar member. If is TRUE and the fParity member is TRUE, replacement occurs. 
function rs_set_flag_null(flag_null) rslib.rscomm_set_flag_null( hport, flag_null ) end -- If is TRUE, null  chars are discarded when received.
function rs_set_rts_control(rts_control) rslib.rscomm_set_rts_control( hport, rts_control ) end -- RTS (request-to-send) flow control. See [3]
function rs_set_abort_on_error(abort_on_error) rslib.rscomm_set_abort_on_error( hport, abort_on_error ) end -- If is TRUE, the driver terminates all read and write operations with an error status if an error occurs. The driver will not accept any further communications operations until the application has acknowledged the error by calling the ClearCommError function.
function rs_set_xon_lim(xon_lim) rslib.rscomm_set_xon_lim( hport, xon_lim ) end -- Minimum number of  chars allowed in the input buffer before flow control is activated to inhibit the sender. Note that the sender may transmit characters after the flow control signal has been activated, so this value should never be zero. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in fInX, fRtsControl, or fDtrControl.
function rs_set_xoff_lim(xoff_lim) rslib.rscomm_set_xoff_lim( hport, xoff_lim ) end -- Maximum number of  chars allowed in the input buffer before flow control is activated to allow transmission by the sender. This assumes that either XON/XOFF, RTS, or DTR input flow control is specified in fInX, fRtsControl, or fDtrControl. The maximum number of unsigned chars allowed is calculated by subtracting this value from the size, in unsigned chars, of the input buffer.
function rs_set_bytesize(bsize) rslib.rscomm_set_bytesize( hport, bsize ) end -- Number of bits in the unsigned chars transmitted and received. 
function rs_set_parity(parity) rslib.rscomm_set_parity( hport, parity ) end -- See [4] 
function rs_set_stop_bits(stop_bits) rslib.rscomm_set_stop_bits( hport, stop_bits ) end -- See [5]

function rs_set_xon_char(xon_char) rslib.rscomm_set_xon_char( hport, xon_char ) end -- Value of the XON character for both transmission and reception. 
function rs_set_xoff_char(xoff_char) rslib.rscomm_set_xoff_char( hport, xoff_char ) end -- Value of the XOFF character for both transmission and reception.
function rs_set_error_char(error_char) rslib.rscomm_set_error_char( hport, error_char ) end -- Value of the character used to replace unsigned chars received with a parity error.
function rs_set_eof_char(eof_char) rslib.rscomm_set_eof_char( hport, eof_char ) end -- Value of the character used to signal the end of data.
function rs_set_evt_char(evt_char) rslib.rscomm_set_evt_char( hport, evt_char ) end -- Value of the character used to signal an event.
function rs_set_rx_timeouts(interval, multiplier, constant) return assert(rslib.rscomm_set_rx_timeouts( hport, interval, multiplier, constant), "Cannot set RX timeouts") end
function rs_set_tx_timeouts(multiplier, constant) return assert(rslib.rscomm_set_tx_timeouts( hport, multiplier, constant), "Cannot set TX timeouts") end
function rs_set_wait_mask(wait_mask) return assert(rslib.rscomm_set_wait_mask( hport,  wait_mask), "Cannot set event mask") end -- See [6]
function rs_set_queue_len(qrx, qtx) return assert(rslib.rscomm_set_queue_len( hport, qrx, qtx ), "Cannot set queue length") end

---------------------------------------------
//rscomm_test.c
#include <windows.h>
#include "rscomm.h" // or using .def file for .dll

#include <stdio.h>



//~ //    import dll:
//~ typedef struct
//~ {
	//~ char *data;
	//~ size_t len;
//~ } iobuffer;    
//~ void* rscomm_open(char* portname);
//~ DCB* rscomm_start_dcb_settings(void*ctx);
//~ void rscomm_set_baudrate(void* ctx, DWORD baudrate);
//~ int rscomm_apply_dcb_settings(void* ctx);
//~ int rscomm_write(void *ctx, iobuffer* buff);

int showError( char* message ){
		char lastError[1024];
		if(message) { printf(message); printf("\n"); }
		
		FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			lastError,
			1024,
			NULL);
		printf(lastError);
		return (BOOL)0;
	}


int main(int argc, char** argv){
    // open port

    void *ctx=rscomm_open("COM3");
    if(!ctx){
        showError("Error opening port");
        goto terminate;
    }

    printf("Port opened");

    // set up
    DCB* dcb=rscomm_start_dcb_settings(ctx);

    rscomm_set_baudrate(ctx, 28800);
    dcb->Parity=NOPARITY;
        
    if(!rscomm_apply_dcb_settings(ctx)) {
        showError("Error configuring port");
        goto terminate;
    }
    
	char command[10]="\x30\x31\x32\x30";
    iobuffer buff;
buff.data=command;
	buff.len=4;

    if(!rscomm_write(ctx, &buff)){
        showError("Error writing to port");
        goto terminate;
    };
        
    // write
terminate:
    // close
    if(!rscomm_close(ctx)) showError("Error closing port");

}
